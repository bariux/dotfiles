### >_<

![desktop](img/s1.png)
---
![terminal](img/s2.png)
---

## how to use :

## Install dependencies
**Debian**
```sh
sudo apt install xorg build-essential libx11-dev libxft-dev libxinerama
```
**Alpine linux**
```sh
apk add git make libx11-dev libxft-dev libxinerama-dev gcc g++ ncurses
```
## Install (compile) dwm,dmenu and st with my config.
```sh
git clone https://gitlab.com/bariux/dotfiles.git
cd dotfiles/
mv ./.xinitrc ../
cd .config/dwm
sudo make ; sudo make install
cd ../dmenu
sudo make ; sudo make install
cd ../st
sudo make ; sudo make install
```

---



